### Docker-Compose[](https://docs.localstack.cloud/getting-started/installation/#docker-compose)

If you want to manually manage your Docker container, it's usually a good idea to use [`docker-compose`](https://docs.docker.com/compose/reference/) in order to simplify your container configuration.

#### Prerequisites[](https://docs.localstack.cloud/getting-started/installation/#prerequisites-2)

-   [`docker`](https://docs.docker.com/get-docker/)
-   [`docker-compose`](https://docs.docker.com/compose/install/) (version 1.9.0+)

#### Starting LocalStack with Docker-Compose[](https://docs.localstack.cloud/getting-started/installation/#starting-localstack-with-docker-compose)



```
$ docker-compose up
```

#### Notes

-   This command pulls the current nightly build from the `master` branch (if you don't have the image locally) and **not** the latest supported version. If you want to use a specific version, set the appropriate localstack image tag at `services.localstack.image` in the `docker-compose.yml` file (for example `localstack/localstack:<version>`).

-   If you are using LocalStack with an [API key](https://docs.localstack.cloud/getting-started/api-key/), you need to specify the image tag as `localstack/localstack-pro`in the `docker-compose.yml` file. Going forward, `localstack/localstack-pro` image will contain our Pro-supported services and APIs.

-   This command reuses the image if it's already on your machine, i.e. it will **not** pull the latest image automatically from Docker Hub.

-   To facilitate interoperability, configuration variables can be prefixed with `LOCALSTACK_` in docker. For instance, setting `LOCALSTACK_PERSISTENCE=1` is equivalent to `PERSISTENCE=1`.

-   Before 0.13: If you do not connect your LocalStack container to the default bridge network with `network_mode: bridge` as in the example, you need to set `LAMBDA_DOCKER_NETWORK=<docker-compose-network>`.

-   If using the Docker default bridge network using `network_mode: bridge`, container name resolution will not work inside your containers. Please consider removing it, if this functionality is needed.

Please note that there's a few pitfalls when configuring your stack manually via docker-compose (e.g., required container name, Docker network, volume mounts, environment variables, etc.). We recommend using the LocalStack CLI to validate your configuration, which will print warning messages in case it detects any (potential) misconfigurations:

```
$ localstack config validate
...
```
